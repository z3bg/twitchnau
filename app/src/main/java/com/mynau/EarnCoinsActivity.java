package com.mynau;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import com.sebastianrask.bettersubscription.R;

import androidx.appcompat.app.AppCompatActivity;

public class EarnCoinsActivity extends AppCompatActivity implements AsyncResponse {
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn_coins);

        Intent i = getIntent();
        user = i.getParcelableExtra("user");
        //Log.v("myNau", user.toString());

        new DownloadImageTask(findViewById(R.id.user_prizes_pic)).execute(user.avatar);

        TextView coinsV = findViewById(R.id.prizes_coins_values);
        coinsV.setText(user.balance.toString());

        TextView userName = findViewById(R.id.prizes_user_name);
        userName.setText(user.name);

        ImageButton back = findViewById(R.id.prizes_back);
        back.setOnClickListener(v -> finish());

        Button paypal = findViewById(R.id.buy_paypal);
        paypal.setOnClickListener(v -> buy(1, 1000));

        Button amazon = findViewById(R.id.buy_amazon);
        amazon.setOnClickListener(v -> buy(2, 9000));

        Button gplay = findViewById(R.id.buy_google);
        gplay.setOnClickListener(v -> buy(3, 20000));
    }

    public void buy(Integer id, Integer cost) {
        if (cost > user.balance) {
            Toast.makeText(this, "You don't have enough coins.", Toast.LENGTH_SHORT).show();
        } else {
            Api.buyPrize buy = new Api.buyPrize();
            buy.delegate = this;
            buy.execute(user.token, id.toString());
        }
    }

    @Override
    public void processFinish(JSONObject output, int resultCode) throws JSONException {
        user.balance = output.getInt("balance");
        TextView coinsV = findViewById(R.id.prizes_coins_values);
        coinsV.setText(user.balance.toString());
        Toast.makeText(this, "Your purchase has been registered. Soon we will get in touch.", Toast.LENGTH_SHORT).show();
    }
}
