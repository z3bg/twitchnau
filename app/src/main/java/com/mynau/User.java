package com.mynau;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    String name;
    String birthdate;
    Integer id;
    String token;
    String avatar;
    Integer balance;
    Integer country;
    String country_name;
    String sex;
    String slug;

    public User(String name, String token) {
        this.name = name;
        this.birthdate = "1970-01-01";
        this.id = 0;
        this.token = token;
        this.avatar = "";
        this.balance = 0;
        this.country = 0;
        this.country_name = "";
        this.sex = "M";
        this.slug = "";
    }

    public String toString() {
        return id + ", " + name + ", " + birthdate + ", " + token + ", " + balance + ", " + avatar + ", " + country + ", " + country_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(birthdate);
        dest.writeInt(id);
        dest.writeString(token);
        dest.writeString(avatar);
        dest.writeInt(balance);
        dest.writeInt(country);
        dest.writeString(country_name);
        dest.writeString(sex);
        dest.writeString(slug);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(Parcel in) {
        name = in.readString();
        birthdate = in.readString();
        id = in.readInt();
        token = in.readString();
        avatar = in.readString();
        balance = in.readInt();
        country = in.readInt();
        country_name = in.readString();
        sex = in.readString();
        slug = in.readString();
    }
}
