package com.mynau;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.sebastianrask.bettersubscription.R;

public class UserProfileActivity extends AppCompatActivity implements AsyncResponseCountries,
        DatePickerFragment.TheListener, AsyncResponseUpdate {

    User user;
    Spinner dropdown, countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        dropdown = findViewById(R.id.spinner1);
        String[] items = new String[]{"Male", "Female", "Trans"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        Intent i = getIntent();
        user = i.getParcelableExtra("user");
        //Log.v("myNau", user.toString());

        if (user != null) {

            new DownloadImageTask(findViewById(R.id.profile_picture)).execute(user.avatar);

            TextView userName = findViewById(R.id.profile_name);
            userName.setText(user.name);
            TextView userBirthdate = findViewById(R.id.profile_birthdate);
            userBirthdate.setText(user.birthdate);

            switch (user.sex) {
                case "M":
                    dropdown.setSelection(0);
                    break;
                case "F":
                    dropdown.setSelection(1);
                    break;
                default:
                    dropdown.setSelection(2);
                    break;
            }
        }

        Api.getCountries getCountries = new Api.getCountries();
        getCountries.delegate = this;
        getCountries.execute(user.token);

        Button save_profile_btn = findViewById(R.id.save_profile);
        save_profile_btn.setOnClickListener(v -> {
            Api.updateProfile updateProfile = new Api.updateProfile();
            updateProfile.delegate = this;
            TextView userName = findViewById(R.id.profile_name);
            String country = user.country_name;
            if (countries != null && countries.getSelectedItem() != null) {
                country = countries.getSelectedItem().toString();
            }
            String sex;
            switch (dropdown.getSelectedItemPosition()) {
                case 0:
                    sex = "M";
                    break;
                case 1:
                    sex = "F";
                    break;
                default:
                    sex = "T";
                    break;
            }
            TextView userBirthdate = findViewById(R.id.profile_birthdate);

            updateProfile.execute(user.token, userName.getText().toString(), country, sex,
                    userBirthdate.getText().toString());
        });

        ImageButton back_profile_btn = findViewById(R.id.profile_back);
        back_profile_btn.setOnClickListener(v -> finish());
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void processFinishCountries(JSONArray output, int resultCode) throws JSONException {
        countries = findViewById(R.id.spinner_countries);
        ArrayList<String> items = new ArrayList<>();
        items.add("Select your country");
        Integer selection = null;
        if (output != null) {
            for (int i = 0; i < output.length(); i++) {
                JSONObject ctr = output.getJSONObject(i);
                items.add(ctr.getString("name"));
                if (user.country != null && user.country == ctr.getInt("id")) {
                    selection = i;
                }
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        countries.setAdapter(adapter);
        if (selection != null) {
            countries.setSelection(selection + 1);
        }
    }

    public void returnBirthDate(String birthdate) {
        TextView userBirthdate = findViewById(R.id.profile_birthdate);
        userBirthdate.setText(birthdate);
    }

    @Override
    public void processFinishUpdate(JSONObject output, int resultCode) {
        finish();
    }
}

