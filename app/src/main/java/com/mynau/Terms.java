package com.mynau;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.sebastianrask.bettersubscription.R;

public class Terms extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        WebView wv = findViewById(R.id.webview);
        wv.setWebViewClient(new SSLTolerentWebViewClient());
        wv.loadUrl("https://www.nau.mobi/terms_mobile");

        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);

        Button acceptTerms = findViewById(R.id.accept_terms);
        acceptTerms.setOnClickListener(view -> {
            finish();
        });

        Button disagreeTerms = findViewById(R.id.disagree_terms);
        disagreeTerms.setOnClickListener(view -> finish());
    }

    private class SSLTolerentWebViewClient extends WebViewClient {
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(Terms.this);
            builder.setMessage("SSL Error");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

}

