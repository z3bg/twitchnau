package com.mynau;

import org.json.JSONArray;
import org.json.JSONException;

public interface AsyncResponseCountries {
    void processFinishCountries(JSONArray output, int resultCode) throws JSONException;
}
