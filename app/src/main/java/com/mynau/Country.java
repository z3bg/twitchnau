package com.mynau;

public class Country {
    public int id;
    public String name;
    public String iso;

    Country(int id, String iso, String name) {
        this.id = id;
        this.iso = iso;
        this.name = name;
    }
}
