package com.mynau;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.net.ssl.HttpsURLConnection;

public class Api {

    private static String host = "https://www.nau.mobi/api/";
    private static String TAG = "myNau";
    private static JSONObject result = null;

    public Api() {}

    public static class doRegistryTask extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        String response = "";
        int status;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;

            try {
                String request = host + "auth/register?name=" + params[0] + "&email=" + params[1] + "&password=" + params[2];
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setDoInput(true);
                httpClient.setDoOutput(true);
                httpClient.setRequestMethod("POST");
                status = httpClient.getResponseCode();
                Log.v(TAG, "Return: " + status);

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                        Log.v(TAG, inputLine);
                    }
                    Log.v(TAG, response);
                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                Log.v(TAG, ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result_local) {
            Log.v(TAG, result.toString());
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class doLoginTask extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        JSONObject result = null;
        int status;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "auth/login?email=" + params[0] + "&password=" + params[1];
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setDoInput(true);
                httpClient.setDoOutput(true);
                httpClient.setRequestMethod("POST");
                status = httpClient.getResponseCode();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            Log.v(TAG, result.toString());
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class doFacebookLogin extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        JSONObject result = null;
        int status;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "auth/FBlogin?token=" + params[0] + "&fb_id=" + params[1];
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setDoInput(true);
                httpClient.setDoOutput(true);
                httpClient.setRequestMethod("POST");
                status = httpClient.getResponseCode();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            }
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class getUserDashboard extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        JSONObject result = null;
        int status;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "me";
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setRequestProperty("Authorization", "Bearer " + params[0]);
                httpClient.setRequestMethod("GET");
                httpClient.setDoInput(true);
                httpClient.setDoOutput(false);
                status = httpClient.getResponseCode();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            }
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class doGoogleLogin extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        JSONObject result = null;
        int status;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "auth/GoogleLogin?email=" + params[0] + "&google_id=" + params[1] +
                        "&name=" + params[2] + "&picture=" + params[3];
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setDoInput(true);
                httpClient.setDoOutput(true);
                httpClient.setRequestMethod("POST");
                status = httpClient.getResponseCode();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            }
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class addImpressionToUser extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        JSONObject result = null;
        int status;
        String resp_txt;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "user/impression?unitId=" + params[0];
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setRequestProperty("Authorization", "Bearer " + params[1]);
                httpClient.setDoInput(true);
                httpClient.setDoOutput(true);
                httpClient.setRequestMethod("POST");
                status = httpClient.getResponseCode();
                resp_txt = httpClient.getResponseMessage();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            } else if (aVoid != null){
                Log.v(TAG, aVoid);
            }
            Log.v(TAG, String.valueOf(status));
            // Log.v(TAG, resp_txt);
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class getCountries extends AsyncTask<String, Void, String> {
        public AsyncResponseCountries delegate = null;
        JSONArray result = null;
        int status;
        String resp_txt;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "countries";
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setRequestProperty("Authorization", "Bearer " + params[0]);
                httpClient.setRequestMethod("GET");
                httpClient.setDoInput(true);
                httpClient.setDoOutput(false);
                status = httpClient.getResponseCode();
                resp_txt = httpClient.getResponseMessage();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONArray(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            } else if (aVoid != null){
                Log.v(TAG, aVoid);
            }
            Log.v(TAG, String.valueOf(status));
            Log.v(TAG, resp_txt);
            try {
                delegate.processFinishCountries(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class updateProfile  extends AsyncTask<String, Void, String> {
        public AsyncResponseUpdate delegate = null;
        JSONObject result = null;
        int status;
        String resp_txt;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "user/profile?name=" + URLEncoder.encode(params[1]) + "&country=" +
                        URLEncoder.encode(params[2]) + "&sex=" + params[3] + "&birthdate=" + URLEncoder.encode(params[4]);

                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setRequestProperty("Authorization", "Bearer " + params[0]);
                httpClient.setDoInput(true);
                httpClient.setDoOutput(true);
                httpClient.setRequestMethod("PUT");
                status = httpClient.getResponseCode();
                resp_txt = httpClient.getResponseMessage();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            } else if (aVoid != null){
                Log.v(TAG, aVoid);
            }
            Log.v(TAG, String.valueOf(status));
            // Log.v(TAG, resp_txt);
            try {
                delegate.processFinishUpdate(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class sendDeleteRequest extends AsyncTask<String, Void, String> {
        public AsyncResponseUpdate delegate = null;
        JSONObject result = null;
        int status;
        String resp_txt;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "user";
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setRequestProperty("Authorization", "Bearer " + params[0]);
                httpClient.setRequestMethod("DELETE");
                httpClient.setDoInput(true);
                httpClient.setDoOutput(false);
                status = httpClient.getResponseCode();
                resp_txt = httpClient.getResponseMessage();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            } else if (aVoid != null){
                Log.v(TAG, aVoid);
            }
            Log.v(TAG, String.valueOf(status));
            Log.v(TAG, resp_txt);
            try {
                delegate.processFinishUpdate(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class buyPrize extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;
        JSONObject result = null;
        int status;
        String resp_txt;

        @Override
        protected String doInBackground(String... params) {

            HttpsURLConnection httpClient;
            String response = "";

            try {
                String request = host + "purchase?prize=" + params[1];
                Log.v(TAG, request);
                URL url = new URL(request);
                httpClient = (HttpsURLConnection) url.openConnection();
                httpClient.setRequestProperty("Authorization", "Bearer " + params[0]);
                httpClient.setRequestMethod("POST");
                httpClient.setDoInput(true);
                httpClient.setDoOutput(false);
                status = httpClient.getResponseCode();
                resp_txt = httpClient.getResponseMessage();

                try {
                    InputStream in = httpClient.getInputStream();
                    BufferedReader input = new BufferedReader(new InputStreamReader(in));
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        response += inputLine;
                    }

                    result = new JSONObject(response);
                    Log.v(TAG, result.toString());
                } finally {
                    httpClient.disconnect();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            if (result != null) {
                Log.v(TAG, result.toString());
            } else if (aVoid != null){
                Log.v(TAG, aVoid);
            }
            Log.v(TAG, String.valueOf(status));
            Log.v(TAG, resp_txt);
            try {
                delegate.processFinish(result, status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

