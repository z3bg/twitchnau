package com.mynau;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
//import android.support.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import com.sebastianrask.bettersubscription.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class helpPage3Activity extends AppCompatActivity implements AsyncResponse, GoogleApiClient.OnConnectionFailedListener {
    private LoginButton loginButton;
    public AsyncResponse FBdelegate = null;
    private CallbackManager callbackManager;
    RelativeLayout loader;
    GoogleSignInClient mGoogleSignInClient;

    private GoogleApiClient mGoogleApiClient;
    int RC_SIGN_IN = 123;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_help_page3);

        sharedPreferences = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        callbackManager = CallbackManager.Factory.create();

        ImageView background = findViewById(R.id.background);
        background.setOnTouchListener(new OnSwipeTouchListener(helpPage3Activity.this) {
            public void onSwipeRight() {
                finish();
            }
            public void onSwipeLeft() {}
        });

        Button clickButton = findViewById(R.id.password_login_button2);
        clickButton.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        });

        loginButton = findViewById(R.id.login_button2);
        loginButton.setReadPermissions("email");
        FBdelegate = this;
        loader = findViewById(R.id.loader2);
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                loader.setVisibility(View.VISIBLE);
                // App code
                Log.v("myNau", "retornou sucesso");
                Api.doFacebookLogin FBlogin = new Api.doFacebookLogin();
                FBlogin.delegate = FBdelegate;
                FBlogin.execute(loginResult.getAccessToken().getToken(), loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("myNau", "retornou cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.v("myNau", exception.toString());
            }
        });

        configureSignIn();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        SignInButton signInButton = findViewById(R.id.sign_in_button2);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        findViewById(R.id.sign_in_button2).setOnClickListener(v -> {
            signIn();
        });
    }

    private void configureSignIn() {
        // Configure sign-in to request the user’s basic profile like name and email
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(…);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, save Token and a state then authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                assert account != null;
                String id = account.getId();
                String name = account.getDisplayName();
                String email = account.getEmail();
                String photo = account.getPhotoUrl().toString();

                loader.setVisibility(View.VISIBLE);
                Api.doGoogleLogin GoogleLogin = new Api.doGoogleLogin();
                GoogleLogin.delegate = FBdelegate;
                GoogleLogin.execute(email, id, name, photo);
            } else {
                // Google Sign In failed, update UI appropriately
                Log.e("myNau", "Login Unsuccessful.");
                Log.e("myNau", result.getStatus().toString());
                Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT)
                        .show();
            }
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void loadUserPanel(String token) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), UserPanelActivity.class);
        intent.putExtra("token", token);
        startActivity(intent);
        finish();
    }

    @Override
    public void processFinish(JSONObject output, int resultCode) {
        try {
            if (output != null) {
                if (output.getBoolean("success")) {
                    JSONObject data = (JSONObject) output.get("data");

                    String token = data.getString("token");

                    Log.e("myNau", "Login Successful. " + token);

                    editor.putString(getString(R.string.token), token);
                    editor.apply();

                    loadUserPanel(token);
                } else {
                    // String message = output.getString("error");
                    // result.setText(message);
                    loader.setVisibility(View.GONE);
                }
            } else {
                // result.setText("Your login is not valid, please try again!");
                LoginManager.getInstance().logOut();
                loader.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // result.setText("An error ocurred, pleae try again in a few seconds... if the problem persist please contact us through our website nau.mobi");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
