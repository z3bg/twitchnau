package com.sebastianrask.bettersubscription.activities.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.BannerCallbacks;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.mynau.Api;
import com.mynau.AsyncResponse;
import com.mynau.DownloadImageTask;
import com.mynau.MainLoginActivity;
import com.rey.material.widget.ProgressView;
import com.sebastianrask.bettersubscription.BuildConfig;
import com.sebastianrask.bettersubscription.R;
import com.sebastianrask.bettersubscription.activities.DonationActivity;
import com.sebastianrask.bettersubscription.activities.ThemeActivity;
import com.sebastianrask.bettersubscription.adapters.MainActivityAdapter;
import com.sebastianrask.bettersubscription.adapters.StreamsAdapter;
import com.sebastianrask.bettersubscription.fragments.NavigationDrawerFragment;
import com.sebastianrask.bettersubscription.misc.TooltipWindow;
import com.sebastianrask.bettersubscription.misc.UniversalOnScrollListener;
import com.sebastianrask.bettersubscription.misc.UpdateDialogHandler;
import com.sebastianrask.bettersubscription.service.AnimationService;
import com.sebastianrask.bettersubscription.service.Service;
import com.sebastianrask.bettersubscription.service.Settings;
import com.sebastianrask.bettersubscription.tasks.ScrollToStartPositionTask;
import com.sebastianrask.bettersubscription.views.recyclerviews.AutoSpanRecyclerView;
import com.sebastianrask.bettersubscription.views.recyclerviews.auto_span_behaviours.AutoSpanBehaviour;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;


public abstract class MainActivity extends ThemeActivity implements AsyncResponse {
    private static final String FIRST_VISIBLE_ELEMENT_POSITION = "firstVisibleElementPosition";

    @BindView(R.id.followed_channels_drawer_layout)
    protected DrawerLayout mDrawerLayout;

    @BindView(R.id.progress_view)
    protected ProgressView mProgressView;

    @BindView(R.id.swipe_container)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.main_list)
    protected AutoSpanRecyclerView mRecyclerView;

    @BindView(R.id.toolbar_shadow)
    protected View mToolbarShadow;

    @BindView(R.id.icon_container)
    protected View mCircleIconWrapper;

    @BindView(R.id.txt_title)
    protected TextView mTitleView;

    @BindView(R.id.error_view)
    protected TextView mErrorView;

    @BindView(R.id.emote_error_view)
    protected TextView mErrorEmoteView;

    @BindView(R.id.img_icon)
    protected ImageView mIcon;

    @BindView(R.id.main_toolbar)
    protected Toolbar mMainToolbar;

    @BindView(R.id.main_decorative_toolbar)
    protected Toolbar mDecorativeToolbar;

    @BindView(R.id.mynau_button)
    protected View mMynauView;

    // The position of the toolbars for the activity that started the transition to this activity
    private float fromToolbarPosition,
            fromMainToolbarPosition;
    private boolean isTransitioned = false;

    protected String LOG_TAG;
    protected MainActivityAdapter mAdapter;
    protected NavigationDrawerFragment mDrawerFragment;
    protected UniversalOnScrollListener mScrollListener;
    protected Settings settings;
    protected TooltipWindow mTooltip;
    protected CastContext mCastContext;

    ImageView coinsIcon;

    private boolean shouldRunOnResumeActions = false;

    /**
     * Refreshes the content of the activity
     */
    public abstract void refreshElements();

    /**
     * Construct the adapter used for this activity's list
     */
    protected abstract MainActivityAdapter constructAdapter(AutoSpanRecyclerView recyclerView);

    /**
     * Get the drawable ressource int used to represent this activity
     *
     * @return the ressource int
     */
    protected abstract int getActivityIconRes();

    /***
     * Get the string ressource int used for the title of this activity
     * @return the ressource int
     */
    protected abstract int getActivityTitleRes();

    /***
     * Construct the AutoSpanBehaviour used for this main activity's AutoSpanRecyclerView
     * @return
     */
    protected abstract AutoSpanBehaviour constructSpanBehaviour();

    /**
     * Allows the child class to specialize the functionality in the onCreate method, although it is not required.
     */
    protected void customizeActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.drawer_fragment);
        settings = new Settings(getBaseContext());

        mCastContext = Service.getShareCastContext(this);
        initErrorView();
        initTitleAndIcon();

        setSupportActionBar(mMainToolbar);
        getSupportActionBar().setTitle("");
        mMainToolbar.setPadding(0, 0, Service.dpToPixels(getBaseContext(), 5), 0); // to make sure the cast icon is aligned 16 dp from the right edge.
        mMainToolbar.bringToFront();
        mMainToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
        mToolbarShadow.bringToFront();
        mToolbarShadow.setAlpha(0f);
        mTitleView.bringToFront();

        // Setup Drawer Fragment
        mDrawerFragment.setUp((DrawerLayout) findViewById(R.id.followed_channels_drawer_layout), mMainToolbar);

        // Set up the RecyclerView
        mRecyclerView.setBehaviour(constructSpanBehaviour());
        mAdapter = constructAdapter(mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(null); // We want to implement our own animations
        mRecyclerView.setHasFixedSize(true);
        mScrollListener = new UniversalOnScrollListener(this, mMainToolbar, mDecorativeToolbar, mToolbarShadow, mCircleIconWrapper, mTitleView, LOG_TAG, true);
        mRecyclerView.addOnScrollListener(mScrollListener);
        mRecyclerView.setHasTransientState(false);

        // Only animate when the view is first started, not when screen rotates
        if (savedInstance == null) {
            mTitleView.setAlpha(0f);
            initActivityAnimation();
        }

        Service.increaseNavigationDrawerEdge(mDrawerLayout, getBaseContext());

        coinsIcon = findViewById(R.id.login_to_my_nau_no_coins_image);
        coinsIcon.setVisibility(View.INVISIBLE);

        // Appodeal
        String appKey = "55cc03810b0afb55a5b130f18bfc8fdcf422b07c8907fac6";
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(this, appKey, Appodeal.BANNER | Appodeal.INTERSTITIAL);
        Appodeal.show(this, Appodeal.BANNER_BOTTOM);

        Appodeal.setBannerCallbacks(new BannerCallbacks() {
            @Override
            public void onBannerLoaded(int height, boolean isPrecache) {
                Log.d("Appodeal", "onBannerLoaded");
            }
            @Override
            public void onBannerFailedToLoad() {
                Log.d("Appodeal", "onBannerFailedToLoad");
            }
            @Override
            public void onBannerShown() {

                Log.d("Appodeal", "onBannerShown");
                String token = MainActivity.this.getUserToken();
                if (token != null) {
                    Api.addImpressionToUser addImpression = new Api.addImpressionToUser();
                    addImpression.delegate = MainActivity.this;
                    addImpression.execute(appKey, token);
                }
            }
            @Override
            public void onBannerClicked() {
                Log.d("Appodeal", "onBannerClicked");
            }
            @Override
            public void onBannerExpired() {
                Log.d("Appodeal", "onBannerExpired");
            }
        });

        ImageView avatar = this.findViewById(R.id.login_to_my_nau_image);
        avatar.setOnClickListener((it -> MainActivity.this.launchNAULogin()));

        if (getFirstOpen() == null) {
            setFirstOpen();
            launchNAULogin();
        }

        mMynauView.setOnClickListener((it -> MainActivity.this.launchNAULogin()));

        checkForTip();
        checkIfUpdated();
        customizeActivity();

      /*  try
        {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName()+"",PackageManager.GET_SIGNATURES);
            for(Signature signature: packageInfo.signatures)
            {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.d("KeyHash",Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();

        shouldRunOnResumeActions = true;

        Appodeal.show(this, Appodeal.BANNER_BOTTOM);
        Appodeal.onResume(this, Appodeal.BANNER | Appodeal.INTERSTITIAL);

        checkIsBackFromMainActivity();

        TextView username = this.findViewById(R.id.login_to_my_nau_no_of_conis);
        username.setText(this.getString(R.string.login));
        username.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        ImageView icon = this.findViewById(R.id.login_to_my_nau_no_coins_image);
        icon.setVisibility(View.GONE);
        ImageView avatar = this.findViewById(R.id.login_to_my_nau_image);
        avatar.setImageResource(R.drawable.nau_icon);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            // Check if the user has changed any size or style setting.
            if (!checkElementStyleChange()) {
                checkElementSizeChange();
            }
        }
        if (hasFocus && shouldRunOnResumeActions) {
            shouldRunOnResumeActions = false;

            String token = this.getUserToken();

            if (token != null) {
                Api.getUserDashboard userDashboard = new Api.getUserDashboard();
                userDashboard.delegate = this;
                userDashboard.execute(token);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mTooltip != null && mTooltip.isTooltipShown()) {
            mTooltip.dismissTooltip();
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        boolean isFromOtherMain = handleBackPressed();

        // If this activity was not started from another main activity, then just use the usual onBackPressed.
        if (!isFromOtherMain) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);

        if (mCastContext != null) {
            MenuItem routeItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu, R.id.media_route_menu_item);
        }
        return true;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (mAdapter.getItemCount() >= 0) {
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        int firstVisibleElement = mRecyclerView.getManager().findFirstCompletelyVisibleItemPosition() == 0
                ? mRecyclerView.getManager().findFirstVisibleItemPosition()
                : mRecyclerView.getManager().findFirstCompletelyVisibleItemPosition();

        outState.putInt(FIRST_VISIBLE_ELEMENT_POSITION, firstVisibleElement);
        super.onSaveInstanceState(outState);
    }

    /**
     * Initializes the error view, but does NOT find the view with ID's
     */
    protected void initErrorView() {
        if (mErrorView != null && mErrorEmoteView != null) {
            mErrorEmoteView.setText(Service.getErrorEmote());
            mErrorEmoteView.setAlpha(0f);
            mErrorView.setAlpha(0f);
        } else {
            throw new IllegalStateException("You need to find the views before you can use them");
        }
    }

    /***
     * Set the title and icon that is used to identify this activity and the content of its list
     */
    protected void initTitleAndIcon() {
        mIcon.setImageResource(getActivityIconRes());
        //mIcon.setImageDrawable(getResources().getDrawable());
        mTitleView.setText(getString(getActivityTitleRes()));
    }

    /**
     * Scrolls to the top of the recyclerview. When the position is reached refreshElements() is called
     */
    public void scrollToTopAndRefresh() {
        ScrollToStartPositionTask scrollTask = new ScrollToStartPositionTask(new ScrollToStartPositionTask.PositionCallBack() {
            @Override
            public void positionReached() {
                if (mRecyclerView != null && mAdapter != null) {
                    refreshElements();
                }
            }

            @Override
            public void cancelled() {

            }
        }, mRecyclerView, mScrollListener);
        scrollTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Shows the error views with an alpha animation
     */
    public void showErrorView() {
        if (mErrorView != null && mErrorEmoteView != null) {
            mErrorEmoteView.setVisibility(View.VISIBLE);
            mErrorView.setVisibility(View.VISIBLE);

            mErrorEmoteView.animate().alpha(1f).start();
            mErrorView.animate().alpha(1f).start();
        }
    }

    /**
     * Hide the error views with an alpha animation
     */
    public void hideErrorView() {
        if (mErrorView != null && mErrorEmoteView != null) {
            mErrorEmoteView.animate().alpha(0f).start();
            mErrorView.animate().alpha(0f).start();
        }
    }

    private void checkIfUpdated() {
        UpdateDialogHandler dialogHandler = new UpdateDialogHandler((ViewGroup) findViewById(android.R.id.content), getLayoutInflater());

        String versionName = BuildConfig.VERSION_NAME;

        if (settings.getIsUpdated() && !settings.getLastVersionName().equals(versionName)) {
            settings.setIsUpdated(false);
            settings.setLastVersionName(versionName);
            //dialogHandler.show();
        }
    }

    /**
     * Check if useablity Tips should be shown to the user
     */
    private void checkForTip() {
        if (!settings.isTipsShown()) {
            try {
                mTooltip = new TooltipWindow(this, TooltipWindow.POSITION_TO_RIGHT);
                mMainToolbar.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        v.removeOnLayoutChangeListener(this);

                        if (!mTooltip.isTooltipShown()) {
                            final View anchor = Service.getNavButtonView(mMainToolbar);
                            if (anchor != null) {
                                anchor.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                                    @Override
                                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                                        mTooltip.showToolTip(anchor, getString(R.string.tip_navigate));
                                    }
                                });
                            }
                        }
                    }
                });
            } catch (Exception e) {
                Log.e(LOG_TAG, "Failed to Show ToolTip");
            }

        }
    }

    /**
     * Checks if the user has changed the element style of this adapter type.
     * If it has Update the adapter element style and refresh the elements.
     */
    public boolean checkElementStyleChange() {
        String currentAdapterStyle = mAdapter.getElementStyle();
        String actualStyle = mAdapter.initElementStyle();

        if (!currentAdapterStyle.equals(actualStyle)) {
            mAdapter.setElementStyle(mAdapter.initElementStyle());
            scrollToTopAndRefresh();
            return true;
        } else {
            return false;
        }
    }

    public boolean checkElementSizeChange() {
        if (mRecyclerView.hasSizedChanged()) {
            scrollToTopAndRefresh();
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns the activity's recyclerview.
     *
     * @return The Recyclerview
     */
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    /**
     * Decides which animation to run based the intent that started the activity.
     */
    public void initActivityAnimation() {
        Intent intent = this.getIntent();
        fromToolbarPosition = intent.getFloatExtra(
                this.getResources().getString(R.string.decorative_toolbar_position_y), -1
        );

        fromMainToolbarPosition = intent.getFloatExtra(
                this.getResources().getString(R.string.main_toolbar_position_y), -1
        );

        // If the position is equal to the default value,
        // then the intent was not put into from another MainActivity
        if (fromToolbarPosition != -1) {
            AnimationService.setActivityToolbarReset(mMainToolbar, mDecorativeToolbar, this, fromToolbarPosition, fromMainToolbarPosition);
        } else {
            AnimationService.setActivityToolbarCircularRevealAnimation(mDecorativeToolbar);
        }

        AnimationService.setActivityIconRevealAnimation(mCircleIconWrapper, mTitleView);
    }

    /**
     * Navigates to the Donation Activity.
     */
    public void navigateToDonationActivity() {
        final Intent intent = new Intent(this, DonationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra(this.getResources().getString(R.string.main_toolbar_position_y), fromMainToolbarPosition);
        intent.putExtra(this.getResources().getString(R.string.decorative_toolbar_position_y), fromToolbarPosition);
        startActivity(intent);
    }

    /**
     * Starts the transition animation to another Main Activity. The method takes an intent where the final result activty has been set.
     * The method puts extra necessary information on the intent before it is started.
     *
     * @param aIntent Intent containing the destination Activity
     */

    public void transitionToOtherMainActivity(final Intent aIntent) {
        hideErrorView();
        GridLayoutManager manager = (GridLayoutManager) mRecyclerView.getLayoutManager();
        final int firstVisibleItemPosition = manager.findFirstVisibleItemPosition();
        final int lastVisibleItemPosition = manager.findLastVisibleItemPosition();

        aIntent.putExtra(
                this.getResources().getString(R.string.decorative_toolbar_position_y),
                mDecorativeToolbar.getTranslationY()
        );

        aIntent.putExtra(
                this.getResources().getString(R.string.main_toolbar_position_y),
                mMainToolbar.getTranslationY()
        );

        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                isTransitioned = true;
                ActivityCompat.startActivity(MainActivity.this, aIntent, null);
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        };

        AnimationService.startAlphaHideAnimation(mCircleIconWrapper);
        AnimationSet alphaHideAnimation = AnimationService.startAlphaHideAnimation(mTitleView);
        if (mRecyclerView.getAdapter().getItemCount() != 0) {
            AnimationService.animateFakeClearing(lastVisibleItemPosition, firstVisibleItemPosition, mRecyclerView, animationListener, mAdapter instanceof StreamsAdapter);
        } else {
            alphaHideAnimation.setAnimationListener(animationListener);
        }
    }

    /**
     * Checks if the user started this activity by pressing the back button on another main activity.
     * If so it runs the show animation for the activity's icon, text and visual elements.
     */
    public void checkIsBackFromMainActivity() {
        if (isTransitioned) {
            GridLayoutManager manager = (GridLayoutManager) mRecyclerView.getLayoutManager();
            final int DELAY_BETWEEN = 50;
            int firstVisibleItemPosition = manager.findFirstVisibleItemPosition();
            int lastVisibleItemPosition = manager.findLastVisibleItemPosition();

            int startPositionCol = AnimationService.getColumnPosFromIndex(firstVisibleItemPosition, mRecyclerView);
            int startPositionRow = AnimationService.getRowPosFromIndex(firstVisibleItemPosition, mRecyclerView);

            // Show the Activity Icon and Text
            AnimationService.startAlphaRevealAnimation(mCircleIconWrapper);
            AnimationService.startAlphaRevealAnimation(mTitleView);

            // Fake fill the RecyclerViews with children again
            for (int i = firstVisibleItemPosition; i <= lastVisibleItemPosition; i++) {
                final View mView = mRecyclerView.getChildAt(i - firstVisibleItemPosition);

                int positionColumnDistance = Math.abs(AnimationService.getColumnPosFromIndex(i, mRecyclerView) - startPositionCol);
                int positionRowDistance = Math.abs(AnimationService.getRowPosFromIndex(i, mRecyclerView) - startPositionRow);
                int delay = (positionColumnDistance + positionRowDistance) * DELAY_BETWEEN;

                //int delay = (i - firstVisibleItemPosition) * DELAY_BETWEEN;
                if (mView != null) {
                    AnimationService.startAlphaRevealAnimation(delay, mView, mAdapter instanceof StreamsAdapter);
                }
            }
            isTransitioned = false;
        }
    }

    /**
     * Starts appropriate animations if the activity has been started by anouther main activity. When the animations end super.onBackPressed() is called.
     * Returns true if that activity has been started through another main activity, else return false;
     */
    public boolean handleBackPressed() {
        if (fromToolbarPosition != -1) {
            Animation.AnimationListener animationListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    try {
                        MainActivity.super.onBackPressed();
                        overridePendingTransition(0, 0);
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            };

            // Animate the Activity Icon and text away
            AnimationService.startAlphaHideAnimation(mCircleIconWrapper);
            AnimationSet alphaHideAnimation = AnimationService.startAlphaHideAnimation(mTitleView);

            GridLayoutManager manager = (GridLayoutManager) mRecyclerView.getLayoutManager();
            final int firstVisibleItemPosition = manager.findFirstVisibleItemPosition();
            final int lastVisibleItemPosition = manager.findLastVisibleItemPosition();
            int duration = (int) alphaHideAnimation.getDuration();
            if (mRecyclerView.getAdapter().getItemCount() != 0) {
                duration = AnimationService.animateFakeClearing(lastVisibleItemPosition, firstVisibleItemPosition, mRecyclerView, animationListener, mAdapter instanceof StreamsAdapter);
            } else {
                alphaHideAnimation.setAnimationListener(animationListener);
            }
            AnimationService.setActivityToolbarPosition(
                    duration,
                    mMainToolbar,
                    mDecorativeToolbar,
                    this,
                    mDecorativeToolbar.getTranslationY(),
                    fromToolbarPosition,
                    mMainToolbar.getTranslationY(),
                    fromMainToolbarPosition
            );

            return true;
        }
        return false;
    }

    /**
     * Functions for myNAU process, login and tokens
     */
//    private final void launchNAULogin()
    private void launchNAULogin() {
        Intent intent = new Intent(this, MainLoginActivity.class);
        Log.v("myNau", "Launch Login");
        Appodeal.hide(this, Appodeal.BANNER);
        this.startActivity(intent);
    }

    @Override
    public void processFinish(@Nullable JSONObject output, int resultCode) throws JSONException {
        if (resultCode == 401) {
            this.removeUserToken();
        }

//            if (appMenu!!.findItem(R.id.action_logout) != null) {
//                val logoutBTN: MenuItem = appMenu!!.findItem(R.id.action_logout)
//                logoutBTN.isVisible = true
//            }

            String userPhoto = output != null ? output.getString("avatar") : null;
            if (userPhoto != null && !userPhoto.equals("")) {
                ImageView avatar = this.findViewById(R.id.login_to_my_nau_image);
                //(new DownloadImageTask(avatar)).execute(userPhoto); doesn't download and put user photo
            }

            String coins = output != null ? output.getString("balance") : null;

            if (coins != null) {
                TextView username = this.findViewById(R.id.login_to_my_nau_no_of_conis);
                username.setText(coins);
                username.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
                ImageView icon = this.findViewById(R.id.btn_coins_nau);
                icon.setVisibility(View.VISIBLE);
                coinsIcon.setVisibility(View.VISIBLE);
            }
        }

    private String getUserToken() {
//        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
//        return preferences.getString(R.string.token.toString(), null)
        return this.getSharedPreferences(this.getString(R.string.token), Context.MODE_PRIVATE)
                .getString(this.getString(R.string.token), null);
    }

    private String getFirstOpen(){
        return this.getSharedPreferences("first_open", Context.MODE_PRIVATE)
                .getString("first_open", null);
    }

    private void setFirstOpen() {
        SharedPreferences.Editor editor = this.getSharedPreferences("first_open", Context.MODE_PRIVATE).edit();
        editor.putString("first_open", "OK").apply(); //it was commit() instead of apply()
    }
    //    private final void removeUserToken()
    private void removeUserToken() {
//        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
//        return preferences.getString(R.string.token.toString(), null)
        this.getSharedPreferences(this.getString(R.string.token), Context.MODE_PRIVATE)
                .edit().remove(this.getString(R.string.token)).apply(); //it was commit() instead of apply()

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            LoginManager.getInstance().logOut();
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            mGoogleSignInClient.signOut();
        }
    }
    public void NAUCLICK(View view)
    {
        launchNAULogin();
    }
}

